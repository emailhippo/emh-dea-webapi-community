﻿// <copyright file="DisposableEmailsController.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Controllers
{
    using System.Threading;
    using System.Threading.Tasks;
    using DeaIdentification.Biz.Consts;
    using DeaIdentification.Biz.Interfaces;
    using DeaIdentification.Models;
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Mvc;
    using Swashbuckle.AspNetCore.Annotations;

    /// <summary>
    /// V1 Disposable Emails Controller.
    /// </summary>
    /// <seealso cref="Controller" />
    [SwaggerTag("Query Disposable Email Address (DEA) records from Wesbos Github repo @ https://github.com/wesbos/burner-email-providers.")]
    [Route("api/dea/v1")]
    [AllowAnonymous]
    [ApiExplorerSettings(GroupName = "v1")]
    [EnableCors("AllowAllOrigins")]
    public sealed class DisposableEmailsController : Controller
    {
        /// <summary>
        /// The disposable domains service.
        /// </summary>
        [NotNull]
        private readonly IDisposableDomainsService<DisposableDomainRequestModel, DisposableDomainResult> disposableDomainsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DisposableEmailsController"/> class.
        /// </summary>
        /// <param name="disposableDomainsService">The disposable domains service.</param>
        public DisposableEmailsController([NotNull] IDisposableDomainsService<DisposableDomainRequestModel, DisposableDomainResult> disposableDomainsService)
        {
            this.disposableDomainsService = disposableDomainsService;
        }

        /// <summary>
        /// Check if the domain is listed in the Wesbos Github list.
        /// </summary>
        /// <param name="query">The domain or email address to query.</param>
        /// <returns>
        /// Return DisposableDomainResult.
        /// </returns>
        /// <response code="400">Bad request. Request is empty/null or exceeds the maximum allowed length of 255 characters.</response>
        /// <response code="429">Maximum processing rate exceeded. Please slow your requests to &lt; 10 queries per second.</response>
        /// <response code="500">Server error.</response>
        [SwaggerOperation("Check if an email address or domain is a Disposable Email Address (DEA).", "Lookup a domain or email address against the Wesbos Github DEA list.")]
        [Produces("application/json", "application/xml")]
        [HttpGet]
        [Route("check/{query}")]
        [ProducesResponseType(typeof(DisposableDomainResult), 200)]
        public async Task<IActionResult> CheckDomain([CanBeNull] string query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                return this.BadRequest(ErrorStates.NoText);
            }

            if (query.Length > 255)
            {
                return this.BadRequest(ErrorStates.QueryTooLong);
            }

            var result = await this.disposableDomainsService.CheckDomainAsync(
                new DisposableDomainRequestModel
            {
                Query = query,
            }, CancellationToken.None);

            if (string.IsNullOrEmpty(result.Result.Error))
            {
                return this.Ok(result);
            }

            return this.BadRequest(result.Result.Error);
        }
    }
}
﻿using DeaIdentification.Infrastructure.Abstractions;
using DeaIdentification.Infrastructure.Consts;
using DeaIdentification.Infrastructure.Helpers;
using DeaIdentification.Infrastructure.Models;
using Microsoft.Extensions.Options;

namespace DeaIdentification.Infrastructure.Services
{
    public class DisposableDomainsService : IDisposableDomainsService
    {
        private readonly IDomainMemoryService _domainMemoryService;
        private readonly IOptions<ApplicationSettings> _applicationSettings;

        public DisposableDomainsService(IDomainMemoryService domainMemoryService,
            IOptions<ApplicationSettings> applicationSettings)
        {
            _domainMemoryService = domainMemoryService;
            _applicationSettings = applicationSettings;
        }

        public DisposableDomainResult CheckDomain(DisposableDomainRequestModel requestModel)
        {
            var result = new DisposableDomainResult
            {
                Doc = _applicationSettings.Value.DomainsGitHubRepositoryUrl,
                Query = requestModel.Query,
            };
            // Validation
            if (string.IsNullOrEmpty(requestModel.Query))
            {
                result.Result.Error = ErrorStates.NoText;
                return result;
            }
            if (ValidationHelper.IsEmailValid(requestModel.Query))
            {
                result.Result.Error = ErrorStates.EmailAddressSupplied;
                return result;
            }
            if (requestModel.Query.Length > 255)
            {
                result.Result.Error = ErrorStates.QueryTooLong;
                return result;
            }
            // Check
            var value = _domainMemoryService.GetValue(requestModel.Query);
            if (!string.IsNullOrEmpty(value))
            {
                result.Result.IsDisposable = true;
            }
            else
            {
                result.Result.IsDisposable = false;
            }
            return result;
        }
    }
}
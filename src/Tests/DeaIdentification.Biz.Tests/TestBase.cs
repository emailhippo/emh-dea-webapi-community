﻿// <copyright file="TestBase.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz.Tests
{
    using System;
    using System.IO;
    using System.Reflection;
    using DeaIdentification.Biz.Bootstrap;
    using EMH.Lib.Patterns.ExceptionProcessor.Interface;
    using EMH.Lib.Patterns.ExceptionProcessor.Logging;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;
    using Serilog;
    using Serilog.Core;
    using Serilog.Events;

    /// <summary>
    ///     The test base.
    /// </summary>
    public abstract class TestBase
    {
        /// <summary>
        ///     This executing assembly.
        /// </summary>
        [NotNull]
        protected static readonly Assembly Assembly = Assembly.GetExecutingAssembly();

        /// <summary>
        /// The logger.
        /// </summary>
        [NotNull]
        private readonly Microsoft.Extensions.Logging.ILogger logger;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TestBase" /> class.
        /// </summary>
        protected TestBase()
        {
            var levelSwitch = new LoggingLevelSwitch { MinimumLevel = LogEventLevel.Verbose };

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(levelSwitch)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();

            this.LoggerFactory = new LoggerFactory().AddSerilog();

            var directoryName = Path.GetDirectoryName(Assembly.GetName().CodeBase);
            if (directoryName != null)
            {
                this.ExecutionPath = directoryName.Replace("file:\\", string.Empty);
            }

            this.logger = this.LoggerFactory.CreateLogger<TestBase>();

            this.ExceptionProcessor = LoggingExceptionProcessorFactory.Create();

            Initializer.Initialize(this.LoggerFactory, this.ExceptionProcessor);
        }

        /// <summary>
        ///     Gets the execution path.
        /// </summary>
        [NotNull]
        protected string ExecutionPath { get; }

        /// <summary>
        /// Gets the exception processor.
        /// </summary>
        /// <value>
        /// The exception processor.
        /// </value>
        [NotNull]
        protected IExceptionProcessor ExceptionProcessor { get; }

        /// <summary>
        /// Gets the logger factory.
        /// </summary>
        /// <value>
        /// The logger factory.
        /// </value>
        [NotNull]
        protected ILoggerFactory LoggerFactory { get; }

        /// <summary>
        /// The write time elapsed.
        /// </summary>
        /// <param name="timerElapsed">
        /// The timer elapsed.
        /// </param>
        protected void WriteTimeElapsed(long timerElapsed)
        {
            this.logger.LogInformation("Elapsed timer: {0}ms", timerElapsed);
        }

        /// <summary>
        /// The write time elapsed.
        /// </summary>
        /// <param name="timerElapsed">
        /// The timer elapsed.
        /// </param>
        protected void WriteTimeElapsed(TimeSpan timerElapsed)
        {
            this.logger.LogInformation("Elapsed timer: {0}", timerElapsed);
        }

        /// <summary>
        /// Gets the source data directory.
        /// </summary>
        /// <param name="testDataFolder">The test data folder.</param>
        /// <returns>The string.</returns>
        [NotNull]
        protected string GetSourceDataDirectory([NotNull] string testDataFolder)
        {
            var parentDir = Directory.GetParent(Directory.GetParent(Directory.GetParent(this.ExecutionPath).FullName).FullName).FullName;

            var sourceDataDirectory = Path.Combine(parentDir, testDataFolder);

            return sourceDataDirectory;
        }
    }
}
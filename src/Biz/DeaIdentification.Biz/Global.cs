﻿// <copyright file="Global.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz
{
    using EMH.Lib.Patterns.ExceptionProcessor.Interface;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Global code infrastructure.
    /// </summary>
    public static class Global
    {
        /// <summary>
        /// Gets or sets the logger factory.
        /// </summary>
        /// <value>
        /// The logger factory.
        /// </value>
        [NotNull]
        public static ILoggerFactory LoggerFactory { get; set; }

        /// <summary>
        /// Gets or sets the exception processor.
        /// </summary>
        /// <value>
        /// The exception processor.
        /// </value>
        [NotNull]
        public static IExceptionProcessor ExceptionProcessor { get; set; }
    }
}
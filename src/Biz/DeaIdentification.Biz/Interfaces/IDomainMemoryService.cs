﻿// <copyright file="IDomainMemoryService.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz.Interfaces
{
    /// <summary>
    /// Domain Memory Service
    /// </summary>
    public interface IDomainMemoryService
    {
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The value.</returns>
        string GetValue(string key);

        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="value">The value.</param>
        void SetValue(string value);
    }
}
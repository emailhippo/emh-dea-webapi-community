﻿using DeaIdentification.Infrastructure.Abstractions;
using DeaIdentification.Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;

namespace DeaIdentification.Controllers
{
    public class DisposableEmailsController : Controller
    {
        private readonly IDisposableDomainsService _disposableDomainsService;

        public DisposableEmailsController(IDisposableDomainsService disposableDomainsService)
        {
            _disposableDomainsService = disposableDomainsService;
        }

        /// <summary>
        /// Check DEA domain GET
        /// </summary>
        /// <returns code="200">Return DisposableDomainResult</returns>
        [HttpGet]
        [Route("api/dea/check")]
        [ProducesResponseType(typeof(DisposableDomainResult), 200)]
        public IActionResult CheckDomain([FromQuery] string query)
        {
            var result = _disposableDomainsService.CheckDomain(new DisposableDomainRequestModel()
            {
                Query = query
            });
            if (string.IsNullOrEmpty(result.Result.Error))
                return Ok(result);
            return BadRequest(result);
        }

        /// <summary>
        /// Check DEA domain POST
        /// </summary>
        /// <returns code="200">Return DisposableDomainResult</returns>
        [HttpPost]
        [Route("api/dea/check")]
        [ProducesResponseType(typeof(DisposableDomainResult), 200)]
        public IActionResult CheckDomain([FromBody] DisposableDomainRequestModel requestModel)
        {
            var result = _disposableDomainsService.CheckDomain(requestModel);
            if (string.IsNullOrEmpty(result.Result.Error))
                return Ok(result);
            return BadRequest(result);
        }
    }
}
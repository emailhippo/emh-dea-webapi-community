﻿// <copyright file="CachePrefixes.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz.Consts
{
    /// <summary>
    /// Cache Prefixes
    /// </summary>
    public static class CachePrefixes
    {
        /// <summary>
        /// Gets the domain.
        /// </summary>
        /// <value>
        /// The domain.
        /// </value>
        public static string Domain => "_Domain_";
    }
}
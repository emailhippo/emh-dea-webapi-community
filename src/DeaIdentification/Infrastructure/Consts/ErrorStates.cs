﻿namespace DeaIdentification.Infrastructure.Consts
{
    public class ErrorStates
    {
        public const string NoText = "No text supplied";
        public const string EmailAddressSupplied = "Email addresses are not supported. Please  enter domain only";
        public const string QueryTooLong = "Query exceeds 255 characters";
    }
}
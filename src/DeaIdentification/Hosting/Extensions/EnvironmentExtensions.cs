﻿using Microsoft.AspNetCore.Hosting;

namespace DeaIdentification.Hosting.Extensions
{
    public static class EnvironmentExtensions
    {
        public static bool IsTesting(this IHostingEnvironment env)
        {
            return env.IsEnvironment(StandardEnvironment.Test);
        }
    }
}
﻿// <copyright file="BsonOutputFormatter.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.MyCode.MediaTypeFormatters.Bson
{
    using System;
    using System.Text;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.Net.Http.Headers;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Bson;

    /// <summary>
    /// A <see cref="BsonOutputFormatter"/> for BSON content
    /// </summary>
    public class BsonOutputFormatter : TextOutputFormatter
    {
        /// <summary>
        /// The json serializer settings
        /// </summary>
        [NotNull]
        private readonly JsonSerializerSettings jsonSerializerSettings;

        /// <summary>
        /// The serializer
        /// </summary>
        [CanBeNull]
        private JsonSerializer serializer;

        /// <summary>
        /// Initializes a new instance of the <see cref="BsonOutputFormatter"/> class.
        /// </summary>
        /// <param name="serializerSettings">The serializer settings.</param>
        /// <exception cref="ArgumentNullException">serializerSettings</exception>
        public BsonOutputFormatter([NotNull] JsonSerializerSettings serializerSettings)
        {
            this.jsonSerializerSettings = serializerSettings;
            this.SupportedEncodings.Add(Encoding.UTF8);
            this.SupportedEncodings.Add(Encoding.Unicode);
            this.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/bson"));
        }

        /// <inheritdoc />
        [NotNull]
        public override Task WriteResponseBodyAsync([NotNull] OutputFormatterWriteContext context, [NotNull] Encoding selectedEncoding)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (selectedEncoding == null)
            {
                throw new ArgumentNullException(nameof(selectedEncoding));
            }

            var response = context.HttpContext.Response;
            using (var bsonWriter = new BsonDataWriter(response.Body) { CloseOutput = false })
            {
                var jsonSerializer = this.CreateJsonSerializer();
                jsonSerializer.Serialize(bsonWriter, context.Object);
                bsonWriter.Flush();
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Creates the json serializer.
        /// </summary>
        /// <returns>The <see cref="JsonSerializer"/>.</returns>
        [NotNull]
        private JsonSerializer CreateJsonSerializer()
        {
            return this.serializer ?? (this.serializer = JsonSerializer.Create(this.jsonSerializerSettings));
        }
    }
}
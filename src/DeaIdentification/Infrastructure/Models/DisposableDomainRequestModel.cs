﻿namespace DeaIdentification.Infrastructure.Models
{
    public class DisposableDomainRequestModel
    {
        public string Query { get; set; }
    }
}
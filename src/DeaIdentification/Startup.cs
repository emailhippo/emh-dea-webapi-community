﻿using System;
using DeaIdentification.Hosting.Extensions;
using DeaIdentification.Infrastructure;
using DeaIdentification.Infrastructure.Abstractions;
using DeaIdentification.Infrastructure.Services;
using DeaIdentification.Scheduler;
using DeaIdentification.Scheduler.Jobs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;

namespace DeaIdentification
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Configuration
            services.AddSingleton(Configuration);
            services.AddOptions();
            services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));
            // Use .NET Core memory cache
            services.AddMemoryCache();
            // Use HttpClient factory
            services.AddHttpClient();
            // Add swagger for development and testing
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "DEA Identification service API"
                });
            });
            // MVC and API services
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            ConnectServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
            IHostingEnvironment env,
            IApplicationLifetime lifetime,
            IServiceScopeFactory serviceScopeFactory,
            ILoggerFactory loggerFactory,
            IServiceProvider serviceProvider)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));

            if (env.IsDevelopment())
            {
                app.UseForwardedHeaders(new ForwardedHeadersOptions
                {
                    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
                    // IIS is also tagging a X-Forwarded-For header on, so we need to increase this limit, 
                    // otherwise the X-Forwarded-For we are passing along from the browser will be ignored
                    ForwardLimit = 2
                });
            }

            if (env.IsDevelopment() || env.IsTesting())
            {
                loggerFactory.AddDebug();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            if (env.IsDevelopment())
            {
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "api/{controller=Home}/{action=Index}/{id?}");
            });
            if (env.IsDevelopment() || env.IsTesting())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1"); });
            }
            var updateEmailsIntervalInMinutes =
                Configuration.GetValue<int>("ApplicationSettings:UpdateEmailsIntervalInMinutes");
            var quartz = new QuartzStartup(serviceScopeFactory);
            lifetime.ApplicationStarted.Register(quartz.Start(jobs =>
            {
                // Register jobs
                jobs.AddJob<UpdateEmailsJob>("UpdateEmailsJob", "EmailJobGroup",
                    TimeSpan.FromMinutes(updateEmailsIntervalInMinutes));
            }));
            lifetime.ApplicationStopping.Register(quartz.Stop);
        }

        private void ConnectServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient(provider => provider.GetService<IHttpContextAccessor>().HttpContext.User);
            services.AddScoped<IDomainMemoryService, DomainMemoryService>();
            services.AddScoped<IDisposableDomainsService, DisposableDomainsService>();
            // Quartz jobs
            services.AddScoped<UpdateEmailsJob>();
        }
    }
}
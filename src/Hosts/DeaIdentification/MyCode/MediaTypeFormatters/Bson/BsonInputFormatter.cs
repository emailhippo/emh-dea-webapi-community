﻿// <copyright file="BsonInputFormatter.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.MyCode.MediaTypeFormatters.Bson
{
    using System.Text;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.Extensions.ObjectPool;
    using Microsoft.Net.Http.Headers;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Bson;
    using Newtonsoft.Json.Serialization;

    /// <summary>
    /// A <see cref="BsonInputFormatter"/> for BSON content
    /// </summary>
    public class BsonInputFormatter : TextInputFormatter
    {
        /// <summary>
        /// The json serializer settings
        /// </summary>
        [NotNull]
        private readonly JsonSerializerSettings jsonSerializerSettings;

        /// <summary>
        /// The object pool provider
        /// </summary>
        [NotNull]
        private readonly ObjectPoolProvider objectPoolProvider;

        /// <summary>
        /// The json serializer pool
        /// </summary>
        [CanBeNull]
        private ObjectPool<JsonSerializer> jsonSerializerPool;

        /// <summary>
        /// Initializes a new instance of the <see cref="BsonInputFormatter"/> class.
        /// </summary>
        /// <param name="jsonSerializerSettings">The json serializer settings.</param>
        /// <param name="objectPoolProvider">The object pool provider.</param>
        public BsonInputFormatter(
            [NotNull] JsonSerializerSettings jsonSerializerSettings,
            [NotNull] ObjectPoolProvider objectPoolProvider)
        {
            this.jsonSerializerSettings = jsonSerializerSettings;
            this.objectPoolProvider = objectPoolProvider;
            this.SupportedEncodings.Add(Encoding.Unicode);
            this.SupportedEncodings.Add(Encoding.UTF8);
            this.SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/bson"));
        }

        /// <inheritdoc />
        [ItemNotNull]
        [NotNull]
        public override Task<InputFormatterResult> ReadRequestBodyAsync([NotNull] InputFormatterContext context, [NotNull] Encoding encoding)
        {
            var request = context.HttpContext.Request;
            using (var reader = new BsonDataReader(request.Body))
            {
                var successful = true;

                void ErrorHandler(object sender, ErrorEventArgs eventArgs)
                {
                    successful = false;
                    var exception = eventArgs.ErrorContext.Error;
                    eventArgs.ErrorContext.Handled = true;
                }

                var jsonSerializer = this.CreateJsonSerializer();
                jsonSerializer.Error += ErrorHandler;
                var type = context.ModelType;
                object model;
                try
                {
                    model = jsonSerializer.Deserialize(reader, type);
                }
                finally
                {
                    this.jsonSerializerPool.Return(jsonSerializer);
                }

                if (successful)
                {
                    return InputFormatterResult.SuccessAsync(model);
                }

                return InputFormatterResult.FailureAsync();
            }
        }

        /// <summary>
        /// Creates the json serializer.
        /// </summary>
        /// <returns>The <see cref="JsonSerializer"/>.</returns>
        [NotNull]
        private JsonSerializer CreateJsonSerializer()
        {
            if (this.jsonSerializerPool == null)
            {
                this.jsonSerializerPool = this.objectPoolProvider.Create(new BsonSerializerObjectPolicy(this.jsonSerializerSettings));
            }

            return this.jsonSerializerPool.Get();
        }
    }
}
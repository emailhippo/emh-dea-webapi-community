#!/bin/sh
#==============================================================================
#title           :run.sh
#description     :This script will run the application.
#author          :roc
#date            :20190923
#version         :1.0
#usage		     :./run.sh
#notes           :
#==============================================================================

## Change to script directory
cd "${0%/*}"

set -ex

main(){
	dotnet DeaIdentification.dll
}

## Execute
main
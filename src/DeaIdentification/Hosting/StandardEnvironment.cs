﻿namespace DeaIdentification.Hosting
{
    public static class StandardEnvironment
    {
        public const string Development = "Development";
        public const string Test = "Testing";
        public const string Staging = "Staging";
        public const string Production = "Production";
    }
}
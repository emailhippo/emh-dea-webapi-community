﻿// <copyright file="MvcBsonSerializerOptionsSetup.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.MyCode.MediaTypeFormatters.Bson
{
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.ObjectPool;
    using Microsoft.Extensions.Options;

    /// <summary>
    /// MVC BSON Serializer OptionsSetup
    /// </summary>
    public class MvcBsonSerializerOptionsSetup : ConfigureOptions<MvcOptions>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MvcBsonSerializerOptionsSetup"/> class.
        /// </summary>
        public MvcBsonSerializerOptionsSetup()
            : base(ConfigureMvc)
        {
        }

        /// <summary>
        /// Configures the MVC.
        /// </summary>
        /// <param name="options">The options.</param>
        private static void ConfigureMvc([NotNull][ItemNotNull] MvcOptions options)
        {
            options.OutputFormatters.Add(new BsonOutputFormatter(
                BsonSerializerSettingsProvider.CreateSerializerSettings()));
            options.InputFormatters.Add(new BsonInputFormatter(
                BsonSerializerSettingsProvider.CreateSerializerSettings(),
                new DefaultObjectPoolProvider()));
        }
    }
}
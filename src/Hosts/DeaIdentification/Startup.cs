﻿// <copyright file="Startup.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Threading;
    using DeaIdentification.Biz.Bootstrap;
    using DeaIdentification.Biz.Config;
    using DeaIdentification.Biz.Interfaces;
    using DeaIdentification.Biz.Services;
    using DeaIdentification.Infrastructure.Services;
    using DeaIdentification.Models;
    using DeaIdentification.MyCode.MediaTypeFormatters.Bson;
    using DeaIdentification.MyCode.MediaTypeFormatters.Protobuf;
    using EMH.Lib.Patterns.ExceptionProcessor.Logging;
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.PlatformAbstractions;
    using Microsoft.Net.Http.Headers;
    using Microsoft.OpenApi.Models;
    using SimpleInjector;

    /// <summary>
    /// Startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// The container.
        /// </summary>
        [NotNull]
        public static readonly Container Container = new Container();

        /// <summary>
        /// The application settings.
        /// </summary>
        [NotNull]
        private readonly ApplicationSettings applicationSettings = new ApplicationSettings();

        /// <summary>
        /// The minimum worker threads.
        /// </summary>
        private readonly int minWorkerThreads;

        /// <summary>
        /// The minimum completion port threads.
        /// </summary>
        private readonly int minCompletionPortThreads;

        /// <summary>
        /// The maximum worker threads.
        /// </summary>
        private readonly int maxWorkerThreads;

        /// <summary>
        /// The maximum completion port threads.
        /// </summary>
        private readonly int maxCompletionPortThreads;

        /// <summary>
        /// The default connection limit.
        /// </summary>
        private readonly int defaultConnectionLimit;

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup([NotNull] IConfiguration configuration)
        {
            ServicePointManager.DefaultConnectionLimit = int.MaxValue;
            ThreadPool.SetMinThreads(100, 100);

            ThreadPool.GetMaxThreads(out this.maxWorkerThreads, out this.maxCompletionPortThreads);
            ThreadPool.GetMinThreads(out this.minWorkerThreads, out this.minCompletionPortThreads);
            this.defaultConnectionLimit = ServicePointManager.DefaultConnectionLimit;

            Configuration = configuration;
            Configuration.Bind("ApplicationSettings", this.applicationSettings);
        }

        /// <summary>
        /// Gets or sets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        private static IConfiguration Configuration { get; set; }

        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <remarks>This method gets called by the runtime. Use this method to add services to the container.</remarks>
        public void ConfigureServices([NotNull] IServiceCollection services)
        {
            /*Add framework services.*/
            services.AddCors(options =>
            {
                options.AddPolicy(
                    "AllowAllOrigins",
                    builder =>
                    {
                        builder.AllowAnyOrigin();
                        builder.AllowAnyHeader();
                        builder.AllowAnyMethod();
                    });
            });

            // Add framework services.
            services
                .AddControllers(
                    options =>
                    {
                        options.OutputFormatters.Clear();
                        options.OutputFormatters.Add(new XmlSerializerOutputFormatter());
                        options.InputFormatters.Add(new ProtobufInputFormatter());
                        options.OutputFormatters.Add(new ProtobufOutputFormatter());
                        options.FormatterMappings.SetMediaTypeMappingForFormat(
                            "protobuf",
                            MediaTypeHeaderValue.Parse("application/x-protobuf"));
                    })
                .AddBsonSerializerFormatters();

            services.AddControllersWithViews();

            services.AddRazorPages();

            services.AddSimpleInjector(
                Container,
                options =>
                {
                    options.AddAspNetCore()
                        .AddControllerActivation()
                        .AddViewComponentActivation()
                        .AddPageModelActivation()
                        .AddTagHelperActivation();
                });

            ConfigureSwagger(services);
        }

        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        /// <param name="logger">The logger.</param>
        /// <remarks>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </remarks>
        public void Configure(
            [NotNull] IApplicationBuilder app,
            [NotNull] IWebHostEnvironment env,
            [NotNull] ILoggerFactory loggerFactory,
            [NotNull] ILogger<Startup> logger)
        {
            logger.LogInformation(1100, "Starting with minWorkerThreads={minWorkerThreads}, minCompletionPortThreads={minCompletionPortThreads}, maxWorkerThreads:{maxWorkerThreads}, maxCompletionPortThreads:{maxCompletionPortThreads}, defaultConnectionLimit:{defaultConnectionLimit}, .", this.minWorkerThreads, this.minCompletionPortThreads, this.maxWorkerThreads, this.maxCompletionPortThreads, this.defaultConnectionLimit);

            var policyCollection = new HeaderPolicyCollection()
                .AddDefaultSecurityHeaders()
                .AddCustomHeader("ARR-Disable-Session-Affinity", "true");

            app.UseSecurityHeaders(policyCollection);

            var exceptionProcessor = LoggingExceptionProcessorFactory.Create();

            /*Bootstrap the app domain*/
            Initializer.Initialize(loggerFactory, exceptionProcessor);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            this.InitializeContainer(app, loggerFactory);

            if (env.IsDevelopment())
            {
                Container.Verify();
            }

            app.UseDefaultFiles();

            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSecondsDefault = 60 * 60 * 12;
                    const int durationInSecondsFarForward = 60 * 60 * 24 * 90;

                    if (ctx.Context.Request.Path.Value.Contains("favicon.ico"))
                    {
                        ctx.Context.Response.Headers[HeaderNames.CacheControl] = string.Format(
                            CultureInfo.InvariantCulture,
                            "public,max-age={0}",
                            durationInSecondsFarForward);
                    }
                    else
                    {
                        ctx.Context.Response.Headers[HeaderNames.CacheControl] = string.Format(
                            CultureInfo.InvariantCulture,
                            "public,max-age={0}",
                            durationInSecondsDefault);
                    }
                },
            });

            app.UseRouting()
                .UseDefaultFiles()
                .UseStatusCodePages();

            app.UseCors(
                a =>
                {
                    a.AllowAnyOrigin();
                    a.AllowAnyHeader();
                    a.AllowAnyMethod();
                });

            app.UseEndpoints(
                endpoints =>
                {
                    endpoints.MapRazorPages();
                    endpoints.MapControllers();
                });

            app.UseSwagger(c => c.SerializeAsV2 = true);

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                c.DefaultModelsExpandDepth(1);
                c.DocumentTitle = @"Email Hippo : Swagger Wesbos DEA Identification Service";
            });
        }

        /// <summary>
        /// Configures the swagger.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        private static void ConfigureSwagger([ItemNotNull][NotNull] IServiceCollection configuration)
        {
            var info = new OpenApiInfo
            {
                Version = "v1",
                Title = "Email Hippo Wesbos DEA Identification API",
                Description = "![](https://d1wiejlotg3vr4.cloudfront.net/bizbranding/co.logos/eh-horiz-695x161.png \"Email Hippo\")<h1>About</h1><p>This project is a free API wrapper for the Disposable Email Address (DEA) list created by [wesbos](https://github.com/wesbos).</p><h1>Possible Use Cases</h1><ul><li>Stopping disposable email addresses from signing up to your blog or forum.</li><li>Risk scoring for on-boarding new clients.</li></ul><h1>Caveats and Limitations</h1><p>This service is hosted by [Email Hippo](https://www.emailhippo.com) and is offered as a free service to The Community. As a free service, there are no Service Level Agreements or other guarantees or warranties provided.</p><h2>Speed and Throttling</h2><p>To ensure fair usage and a good performance experience for everyone, there are controls in place to restrict the speed at which you can use this service. The maximum speed permitted is 10 queries per second - after that the service will return HTTP code 429 (slow down) for a duration of 1 minute.</p><p><strong>Please note:</strong> The data returned is derived from the wesbos Github repo and while there are a lot of domains on the list, this is only a small subset of the Disposable Email Address domains known to Email Hippo in it's commercial offering. That being said, the wesbos list is fairly good and certainly better than having nothing in place to protect your sign up against from abuse from DEAs.</p><h1>Support</h1><p>Officially, this is an unsupported service that is offered on a best efforts basis and can be withdrawn at any time. However, if you have any issues or suggestions, please feel free to raise these on the Github repo and we'll look into it as soon as we are able.</p><h1>Related resources</h1><ul><li>[Wesbos Github Repo](https://github.com/wesbos/burner-email-providers)</li><li>[Implementation Documentation](#)</li><li>[Email Hippo Full Email Validation API (fully supported and includes advanced DEA detection)](https://www.emailhippo.com/products/more/)</li><li>[Support and Suggestions](https://github.com/wesbos/burner-email-providers)</li></ul>",
            };

            configuration.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", info);

                var filePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "DeaIdentification.xml");
                c.IncludeXmlComments(filePath);
                c.IgnoreObsoleteActions();
                c.EnableAnnotations();
            });
        }

        /// <summary>
        /// Initializes the container.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        private void InitializeContainer(
            [NotNull] IApplicationBuilder app,
            [NotNull] ILoggerFactory loggerFactory)
        {
            Container.RegisterSingleton<IHttpContextAccessor, HttpContextAccessor>();
            Container.RegisterSingleton(() => loggerFactory);

            Container.RegisterSingleton(() => this.applicationSettings);

            Container.RegisterSingleton<IDisposableDomainsService<DisposableDomainRequestModel, DisposableDomainResult>, DisposableDomainsService>();
            Container.RegisterSingleton<ICacheService<List<string>>, WesbosGithubCacheService>();
        }
    }
}
﻿// <copyright file="IDisposableDomainsService.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz.Interfaces
{
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// Disposable Domains Service
    /// </summary>
    /// <typeparam name="TRequest">The type of the request.</typeparam>
    /// <typeparam name="TResponse">The type of the response.</typeparam>
    public interface IDisposableDomainsService<in TRequest, TResponse>
    {
        /// <summary>
        /// Checks the domain.
        /// </summary>
        /// <param name="requestModel">The request model.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>
        /// The response.
        /// </returns>
        [NotNull]
        Task<TResponse> CheckDomainAsync([NotNull] TRequest requestModel, CancellationToken cancellationToken);
    }
}
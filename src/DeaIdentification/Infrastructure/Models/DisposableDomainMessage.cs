﻿namespace DeaIdentification.Infrastructure.Models
{
    public class DisposableDomainMessage
    {
        public bool IsDisposable { get; set; }
        public string Error { get; set; }
    }
}
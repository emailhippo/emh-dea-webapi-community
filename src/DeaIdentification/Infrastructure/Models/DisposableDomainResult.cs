﻿namespace DeaIdentification.Infrastructure.Models
{
    public class DisposableDomainResult
    {
        public string Doc { get; set; }
        public string Query { get; set; }
        public DisposableDomainMessage Result { get; set; } = new DisposableDomainMessage();
    }
}
﻿// <copyright file="MvcBsonMvcBuilderExtensions.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.MyCode.MediaTypeFormatters.Bson
{
    using System;
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using Microsoft.Extensions.Options;

    /// <summary>
    /// MVC BSON MVC Builder Extensions
    /// </summary>
    public static class MvcBsonMvcBuilderExtensions
    {
        /// <summary>
        /// Adds the bson serializer formatters.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <returns><see cref="IMvcBuilder"/>.</returns>
        /// <exception cref="ArgumentNullException">builder</exception>
        [NotNull]
        public static IMvcBuilder AddBsonSerializerFormatters([NotNull] this IMvcBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.Services.TryAddEnumerable(
                ServiceDescriptor.Transient<IConfigureOptions<MvcOptions>, MvcBsonSerializerOptionsSetup>());
            return builder;
        }
    }
}
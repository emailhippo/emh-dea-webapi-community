﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using DeaIdentification.Infrastructure;
using DeaIdentification.Infrastructure.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Quartz;

namespace DeaIdentification.Scheduler.Jobs
{
    public class UpdateEmailsJob : IJob
    {
        private readonly ILogger<UpdateEmailsJob> _logger;
        private readonly IOptions<ApplicationSettings> _applicationSettings;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IDomainMemoryService _domainMemoryService;

        public UpdateEmailsJob(ILogger<UpdateEmailsJob> logger,
            IOptions<ApplicationSettings> applicationSettings,
            IHttpClientFactory httpClientFactory, 
            IDomainMemoryService domainMemoryService)
        {
            _domainMemoryService = domainMemoryService;
            _httpClientFactory = httpClientFactory;
            _domainMemoryService = domainMemoryService;
            _logger = logger;
            _applicationSettings = applicationSettings;
        }

        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                var url = _applicationSettings.Value.DomainsTextFileUrl;
                var httpClient = _httpClientFactory.CreateClient();
                httpClient.Timeout = TimeSpan.FromMilliseconds(Timeout.Infinite);
                var stream = httpClient.GetStreamAsync(url).Result;
                using (var reader = new StreamReader(stream))
                {
                    while (!reader.EndOfStream)
                    {
                        var currentLine = reader.ReadLine();
                        if (!string.IsNullOrEmpty(currentLine))
                        {
                            _domainMemoryService.SetValue(currentLine);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error while obtaining domain list");
                Console.WriteLine(e);
            }

            return null;
        }
    }
}
﻿// <copyright file="EmailAddressHelper.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz.Helpers
{
    using JetBrains.Annotations;

    /// <summary>
    /// Email Address Helper
    /// </summary>
    public static class EmailAddressHelper
    {
        /// <summary>
        /// The splitter
        /// </summary>
        [NotNull]
        private static readonly char[] Splitter = { '@' };

        /// <summary>
        /// Parses the domain.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>Item1: User, Item2: Domain</returns>
        [CanBeNull]
        public static (string, string)? ParseDomain([CanBeNull] this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return null;
            }

            if (!input.Contains("@"))
            {
                return (null, input);
            }

            var strings = input.Split(Splitter);

            if (strings.Length >= 2)
            {
                return (strings[0], strings[strings.Length - 1]);
            }

            return (null, input);
        }
    }
}
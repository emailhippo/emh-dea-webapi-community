﻿// <copyright file="HomeController.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Controllers
{
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Home Controller.
    /// </summary>
    /// <seealso cref="Controller" />
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : Controller
    {
        /// <summary>
        /// Home index page.
        /// </summary>
        /// <returns>The <see cref="IActionResult"/>.</returns>
        [HttpGet]
        [Route("~/")]
        [ResponseCache(VaryByHeader = "User-Agent", Duration = 3600)]
        [NotNull]
        public IActionResult Index()
        {
            return this.Ok("Email Hippo API for Wesbos DEA list. See https://github.com/wesbos/burner-email-providers for further information.");
        }

        /// <summary>
        /// Errors this instance.
        /// </summary>
        /// <returns>Error page.</returns>
        [HttpGet]
        [Route("~/Error")]
        [NotNull]
        public IActionResult Error()
        {
            return this.StatusCode(500, "server error");
        }
    }
}
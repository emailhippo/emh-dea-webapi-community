﻿// <copyright file="BsonSerializerSettingsProvider.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.MyCode.MediaTypeFormatters.Bson
{
    using JetBrains.Annotations;
    using Newtonsoft.Json;

    /// <summary>
    /// BSON Serializer Settings Provider
    /// </summary>
    public static class BsonSerializerSettingsProvider
    {
        /// <summary>
        /// Creates the serializer settings.
        /// </summary>
        /// <returns>The <see cref="JsonSerializerSettings"/>.</returns>
        [NotNull]
        public static JsonSerializerSettings CreateSerializerSettings()
        {
            return new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Ignore,
                TypeNameHandling = TypeNameHandling.None
            };
        }
    }
}
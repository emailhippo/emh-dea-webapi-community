﻿// <copyright file="Program.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification
{
    using System;
    using System.Threading.Tasks;
    using EMH.Lib.Patterns.HippoLog.Core;
    using EMH.Lib.Patterns.HippoLog.SerilogAdapter;
    using Microsoft.AspNetCore.DataProtection;
    using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption;
    using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Serilog;

    /// <summary>
    /// The program.
    /// </summary>
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class Program
    {
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static async Task Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateBootstrapLogger();

            try
            {
                await CreateHostBuilder(args).Build().RunAsync();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "An unhandled exception occured during bootstrapping");
            }
            finally
            {
                await Log.CloseAndFlushAsync();
            }
        }

        /// <summary>
        /// Creates the host builder.
        /// </summary>
        /// <param name="args">The program args.</param>
        /// <returns>The <see cref="IHostBuilder"/>.</returns>
        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder(args)
                .UseSerilog((context, configuration) =>
                {
                    var contextConfiguration = context.Configuration;
                    var loggerConfiguration = configuration;

                    loggerConfiguration
                        .ReadFrom.Configuration(contextConfiguration);

                    var serilogAdapter = SerilogAdapterFactory.Create();

                    HippoLog.Initialize(serilogAdapter);
                })
                .ConfigureLogging(
                    (hostingContext, logging) =>
                {
                    var hostingContextHostingEnvironment = hostingContext.HostingEnvironment;

                    logging
                        .AddConfiguration(hostingContext.Configuration.GetSection("Logging"))
                        .AddConsole();

                    if (hostingContextHostingEnvironment.IsDevelopment())
                    {
                        logging.AddDebug();
                    }

                    logging.AddSerilog();
                })
                .ConfigureWebHostDefaults(
                    builder =>
                {
                    builder.ConfigureServices(
                            services =>
                    {
                        services.AddDataProtection()
                            .UseCryptographicAlgorithms(
                                new AuthenticatedEncryptorConfiguration
                                {
                                    EncryptionAlgorithm = EncryptionAlgorithm.AES_256_CBC,
                                    ValidationAlgorithm = ValidationAlgorithm.HMACSHA256,
                                });
                    })
                    .UseStartup<Startup>()
                    .UseKestrel(options =>
                    {
                        options.AddServerHeader = false;
                    })
                    .UseSentry();
                });
    }
}
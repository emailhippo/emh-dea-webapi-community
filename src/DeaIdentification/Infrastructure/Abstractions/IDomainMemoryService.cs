﻿namespace DeaIdentification.Infrastructure.Abstractions
{
    public interface IDomainMemoryService
    {
        string GetValue(string key);
        void SetValue(string value);
    }
}
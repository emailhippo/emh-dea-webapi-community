﻿// <copyright file="ProtobufInputFormatter.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.MyCode.MediaTypeFormatters.Protobuf
{
    using System;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.Net.Http.Headers;
    using ProtoBuf.Meta;

    /// <summary>
    /// Protobuf Input Formatter.
    /// </summary>
    /// <seealso cref="InputFormatter" />
    public class ProtobufInputFormatter : InputFormatter
    {
        /// <summary>
        /// The model value.
        /// </summary>
        [ItemNotNull]
        [NotNull]
        private static readonly Lazy<RuntimeTypeModel> ModelValue = new Lazy<RuntimeTypeModel>(CreateTypeModel);

        /// <summary>
        /// Gets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        [NotNull]
        public static RuntimeTypeModel Model { get; } = ModelValue.Value;

        /// <inheritdoc />
        [ItemNotNull]
        [NotNull]
        public override Task<InputFormatterResult> ReadRequestBodyAsync([NotNull] InputFormatterContext context)
        {
            var type = context.ModelType;
            var request = context.HttpContext.Request;
            MediaTypeHeaderValue.TryParse(request.ContentType, out _);

            object result = Model.Deserialize(context.HttpContext.Request.Body, null, type);
            return InputFormatterResult.SuccessAsync(result);
        }

        /// <inheritdoc />
        public override bool CanRead([NotNull] InputFormatterContext context)
        {
            return true;
        }

        /// <summary>
        /// Creates the type model.
        /// </summary>
        /// <returns>The <see cref="RuntimeTypeModel"/>.</returns>
        [NotNull]
        private static RuntimeTypeModel CreateTypeModel()
        {
            var typeModel = RuntimeTypeModel.Create();
            typeModel.UseImplicitZeroDefaults = false;
            return typeModel;
        }
    }
}
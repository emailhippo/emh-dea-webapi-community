﻿// <copyright file="DisposableDomainMessage.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Models
{
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Disposable Domain Message.
    /// </summary>
    [DataContract]
    [ProtoContract]
    public sealed class DisposableDomainMessage
    {
        /// <summary>
        /// Whether this domain is a disposable domain.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the domain is disposable email address provider; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty("isDisposable", Order = 1)]
        [DataMember(Order = 1)]
        [ProtoMember(1)]
        public bool IsDisposable { get; set; }

        /// <summary>
        /// If there is an error, the message is described here.
        /// </summary>
        /// <value>
        /// If there is an error, the message is described here.
        /// </value>
        [JsonProperty("error", Order = 2)]
        [DataMember(Order = 2, IsRequired = true)]
        [ProtoMember(2)]
        public string Error { get; set; }
    }
}
﻿namespace DeaIdentification.Infrastructure
{
    public class ApplicationSettings
    {
        public string DomainsTextFileUrl { get; set; }
        public string DomainsGitHubRepositoryUrl { get; set; }
        public int AbsoluteExpirationInMinutes { get; set; }
        public int UpdateEmailsIntervalInMinutes { get; set; }
    }
}
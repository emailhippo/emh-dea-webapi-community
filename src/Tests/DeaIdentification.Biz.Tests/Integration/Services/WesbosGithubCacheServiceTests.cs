﻿// <copyright file="WesbosGithubCacheServiceTests.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz.Tests.Integration.Services
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using DeaIdentification.Biz.Config;
    using DeaIdentification.Biz.Interfaces;
    using DeaIdentification.Biz.Services;
    using JetBrains.Annotations;
    using NUnit.Framework;

    /// <summary>
    /// The <see cref="WesbosGithubCacheServiceTests"/>.
    /// </summary>
#if !DEBUG
    [Ignore("Do not run on build server")]
#endif
    [TestFixture]
    public class WesbosGithubCacheServiceTests : TestBase
    {
        /// <summary>
        /// The sut.
        /// </summary>
        [NotNull]
        private static ICacheService<List<string>> sut;

        /// <summary>
        /// Initializes a new instance of the <see cref="WesbosGithubCacheServiceTests"/> class.
        /// </summary>
        public WesbosGithubCacheServiceTests()
        {
            sut = new WesbosGithubCacheService(new ApplicationSettings { AbsoluteExpirationInMinutes = 10, DomainsTextFileUrl = @"https://raw.githubusercontent.com/wesbos/burner-email-providers/master/emails.txt" });
        }

        /// <summary>
        /// Gets the or set asynchronous when pointed at repo expect items.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        [Repeat(10)]
        public async Task GetOrSetAsync_WhenPointedAtRepo_ExpectItems()
        {
            // Arrange

            // Act
            var stopwatch = Stopwatch.StartNew();
            var list = await sut.GetOrSetAsync(CancellationToken.None).ConfigureAwait(false);
            stopwatch.Stop();

            // Assert
            Assert.NotNull(list);
            Assert.True(list.Any());
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }
    }
}
﻿using DeaIdentification.Infrastructure.Models;

namespace DeaIdentification.Infrastructure.Abstractions
{
    public interface IDisposableDomainsService
    {
        DisposableDomainResult CheckDomain(DisposableDomainRequestModel requestModel);
    }
}
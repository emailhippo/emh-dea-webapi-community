﻿// <copyright file="DisposableDomainRequestModel.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Models
{
    using System.Runtime.Serialization;
    using JetBrains.Annotations;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Disposable Domain Request Model.
    /// </summary>
    [DataContract]
    [ProtoContract]
    public class DisposableDomainRequestModel
    {
        /// <summary>
        /// Gets or sets the query.
        /// </summary>
        /// <value>
        /// The query.
        /// </value>
        [JsonProperty("query", Order = 1)]
        [DataMember(Order = 1)]
        [ProtoMember(1)]
        [CanBeNull]
        public string Query { get; set; }
    }
}
﻿// <copyright file="ApplicationSettings.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz.Config
{
    /// <summary>
    /// Application Settings
    /// </summary>
    public sealed class ApplicationSettings
    {
        /// <summary>
        /// Gets or sets the domains text file URL.
        /// </summary>
        /// <value>
        /// The domains text file URL.
        /// </value>
        public string DomainsTextFileUrl { get; set; }

        /// <summary>
        /// Gets or sets the domains git hub repository URL.
        /// </summary>
        /// <value>
        /// The domains git hub repository URL.
        /// </value>
        public string DomainsGitHubRepositoryUrl { get; set; }

        /// <summary>
        /// Gets or sets the absolute expiration in minutes.
        /// </summary>
        /// <value>
        /// The absolute expiration in minutes.
        /// </value>
        public int AbsoluteExpirationInMinutes { get; set; }

        /// <summary>
        /// Gets or sets the update emails interval in minutes.
        /// </summary>
        /// <value>
        /// The update emails interval in minutes.
        /// </value>
        public int UpdateEmailsIntervalInMinutes { get; set; }
    }
}
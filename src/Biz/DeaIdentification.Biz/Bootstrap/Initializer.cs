﻿// <copyright file="Initializer.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz.Bootstrap
{
    using EMH.Lib.Patterns.ExceptionProcessor.Interface;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// The <see cref="Initializer"/>.
    /// </summary>
    public static class Initializer
    {
        /// <summary>
        /// Initializes the specified logger factory.
        /// </summary>
        /// <param name="loggerFactory">The logger factory.</param>
        /// <param name="exceptionProcessor">The exception processor.</param>
        public static void Initialize(
            [NotNull] ILoggerFactory loggerFactory,
            [NotNull] IExceptionProcessor exceptionProcessor)
        {
            Global.LoggerFactory = loggerFactory;
            Global.ExceptionProcessor = exceptionProcessor;
        }
    }
}
﻿// <copyright file="ICacheService.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz.Interfaces
{
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;

    /// <summary>
    /// The cache service
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface ICacheService<TEntity>
    {
        /// <summary>
        /// Gets the object specified by the cache key.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>
        /// The cached object or null if not in cache.
        /// </returns>
        [ItemCanBeNull]
        [NotNull]
        Task<TEntity> GetOrSetAsync(CancellationToken cancellationToken);
    }
}
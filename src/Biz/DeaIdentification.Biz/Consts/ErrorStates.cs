﻿// <copyright file="ErrorStates.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz.Consts
{
    /// <summary>
    /// Error States
    /// </summary>
    public sealed class ErrorStates
    {
        /// <summary>
        /// The no text
        /// </summary>
        public const string NoText = "No text supplied";

        /// <summary>
        /// The email address supplied
        /// </summary>
        public const string EmailAddressSupplied = "Email addresses are not supported. Please enter domain only.";

        /// <summary>
        /// The query too long
        /// </summary>
        public const string QueryTooLong = "Query exceeds 255 characters";
    }
}
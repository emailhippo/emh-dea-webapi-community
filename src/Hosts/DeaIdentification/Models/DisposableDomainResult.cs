﻿// <copyright file="DisposableDomainResult.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Models
{
    using System.Runtime.Serialization;
    using JetBrains.Annotations;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// The result that shows whether the domain is disposable together with any error information.
    /// </summary>
    [DataContract]
    [ProtoContract]
    public sealed class DisposableDomainResult
    {
        /// <summary>
        /// The documentation URL.
        /// </summary>
        /// <value>
        /// The documentation URL.
        /// </value>
        [JsonProperty("doc", Order = 1)]
        [DataMember(Order = 1)]
        [ProtoMember(1)]
        [CanBeNull]
        public string Doc { get; set; }

        /// <summary>
        /// The query (i.e the domain name to check against the main wesbos list).
        /// </summary>
        /// <value>
        /// The query (i.e the domain name to check against the main wesbos list).
        /// </value>
        [JsonProperty("query", Order = 2)]
        [DataMember(Order = 2)]
        [ProtoMember(2)]
        [CanBeNull]
        public string Query { get; set; }

        /// <summary>
        /// The result of the checking against the mail wesbos list.
        /// </summary>
        /// <value>
        /// The <see cref="DisposableDomainMessage"/>.
        /// </value>
        [JsonProperty("result", Order = 3)]
        [DataMember(Order = 3)]
        [ProtoMember(3)]
        [CanBeNull]
        public DisposableDomainMessage Result { get; set; }
    }
}
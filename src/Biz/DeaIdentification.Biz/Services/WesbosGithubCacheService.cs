﻿// <copyright file="WesbosGithubCacheService.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Biz.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using DeaIdentification.Biz.Config;
    using DeaIdentification.Biz.Interfaces;
    using EMH.Lib.Patterns.ExceptionProcessor.Interface;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Wesbos Github Cache Service.
    /// </summary>
    public sealed class WesbosGithubCacheService : ICacheService<List<string>>
    {
        /// <summary>
        /// The cache key.
        /// </summary>
        private const string CacheKey = @"B8D5D54A-ACF9-483A-945F-905671257362";

        /// <summary>
        /// My memory cache.
        /// </summary>
        [NotNull]
        private static readonly MemoryCache MyMemoryCache = new MemoryCache(new MemoryCacheOptions());

        /// <summary>
        /// The wesbos github client.
        /// </summary>
        [NotNull]
        private static readonly HttpClient WesbosGithubClient = new HttpClient { Timeout = TimeSpan.FromSeconds(60) };

        /// <summary>
        /// The reversion.
        /// </summary>
        /*Note: no need to make this thread safe as infrequently hit.*/
        [CanBeNull]
        private static List<string> reversion;

        /// <summary>
        /// The settings.
        /// </summary>
        [NotNull]
        private readonly ApplicationSettings settings;

        /// <summary>
        /// The logger.
        /// </summary>
        [NotNull]
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="WesbosGithubCacheService"/> class.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public WesbosGithubCacheService([NotNull] ApplicationSettings settings)
        {
            this.logger = Global.LoggerFactory.CreateLogger<WesbosGithubCacheService>();
            this.settings = settings;
        }

        /// <inheritdoc />
        public async Task<List<string>> GetOrSetAsync(CancellationToken cancellationToken)
        {
            var list = await MyMemoryCache.GetOrCreateAsync(CacheKey, async _ =>
            {
                var cacheValue = _.Value;
                if (cacheValue != null)
                {
                    var value = (List<string>)cacheValue;

                    return value;
                }

                _.AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(this.settings.AbsoluteExpirationInMinutes);
                var newItems = await this.GetCurrentDataAsync(cancellationToken).ConfigureAwait(false);
                var list1 = newItems?.ToList();
                _.Value = list1;

                if (list1 == null || !list1.Any())
                {
                    this.logger.LogError("Items from GitHub fetch are null. There is a problem! Consult exception logs for further details.");
                }
                else
                {
                    this.logger.LogTrace("Cache refresh event. {0} items loaded into cache.", list1.Count);
                }

                return list1;
            }).ConfigureAwait(false);

            return list;
        }

        /// <summary>
        /// Gets the current data asynchronous.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>
        /// A <see cref="Task" /> representing the asynchronous operation.
        /// </returns>
        [NotNull]
        [ItemCanBeNull]
        private async Task<IEnumerable<string>> GetCurrentDataAsync(CancellationToken cancellationToken)
        {
            HttpResponseMessage responseMessage = null;

            string remoteString = null;
            var rtn = new List<string>();
            var failed = false;

            try
            {
                responseMessage = await WesbosGithubClient.GetAsync(this.settings.DomainsTextFileUrl, cancellationToken).ConfigureAwait(false);

                if (responseMessage.IsSuccessStatusCode)
                {
                    remoteString = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
            }
            catch (Exception exception)
            {
                failed = true;
                Global.ExceptionProcessor.HandleException(exception, ExceptionProcessorPolicies.LogAndResumePolicy);
                if (reversion != null && reversion.Any())
                {
                    this.logger.LogWarning("Remote fetch from {0} failed. Using previous version instead.", this.settings.DomainsTextFileUrl);
                    rtn = reversion;
                }
            }
            finally
            {
                responseMessage?.Dispose();
            }

            if (failed)
            {
                return rtn;
            }

            using (var sr = new StringReader(remoteString))
            {
                string line = null;

                do
                {
                    line = await Global.ExceptionProcessor.ProcessAsync(() => sr.ReadLineAsync(), ExceptionProcessorPolicies.LogAndResumePolicy).ConfigureAwait(false);

                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        rtn.Add(line.Trim().ToLowerInvariant());
                    }
                }
                while (!string.IsNullOrWhiteSpace(line));
            }

            if (reversion == null)
            {
                /*Store away current good for future use in event ouf temporary Github outage.*/
                reversion = new List<string>();
            }

            reversion = rtn;

            return rtn;
        }
    }
}
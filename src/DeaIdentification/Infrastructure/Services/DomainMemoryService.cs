﻿using System;
using DeaIdentification.Infrastructure.Abstractions;
using DeaIdentification.Infrastructure.Consts;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

namespace DeaIdentification.Infrastructure.Services
{
    public class DomainMemoryService : IDomainMemoryService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly MemoryCacheEntryOptions _memoryCacheEntryOptions;

        public DomainMemoryService(IMemoryCache cache, 
            IOptions<ApplicationSettings> applicationSettings)
        {
            _memoryCache = cache;
            _memoryCacheEntryOptions =
                CreateMemoryCacheEntryOptions(applicationSettings.Value.AbsoluteExpirationInMinutes);
        }

        private static MemoryCacheEntryOptions CreateMemoryCacheEntryOptions(int absoluteExpirationInMinutes)
        {
            if (absoluteExpirationInMinutes <= 0)
            {
                absoluteExpirationInMinutes = 60;
            }

            return new MemoryCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(absoluteExpirationInMinutes),
                Priority = CacheItemPriority.Normal,
            };
        }

        public string GetValue(string key)
        {
            var cacheKey = CachePrefixes.Domain + key;
            // Look for cache key.
            if (!_memoryCache.TryGetValue(cacheKey, out string cacheEntry))
            {
                cacheEntry = null;
            }

            return cacheEntry;
        }

        public void SetValue(string value)
        {
            _memoryCache.Set(CachePrefixes.Domain + value, value, _memoryCacheEntryOptions);
        }
    }
}
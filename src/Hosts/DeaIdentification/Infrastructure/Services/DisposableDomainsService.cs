﻿// <copyright file="DisposableDomainsService.cs" company="Email Hippo Ltd">
// Copyright (c) Email Hippo Ltd. All rights reserved.
// </copyright>

namespace DeaIdentification.Infrastructure.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using DeaIdentification.Biz.Config;
    using DeaIdentification.Biz.Consts;
    using DeaIdentification.Biz.Helpers;
    using DeaIdentification.Biz.Interfaces;
    using DeaIdentification.Models;
    using JetBrains.Annotations;

    /// <summary>
    /// Disposable Domains Service.
    /// </summary>
    public sealed class DisposableDomainsService : IDisposableDomainsService<DisposableDomainRequestModel, DisposableDomainResult>
    {
        /// <summary>
        /// The domain memory service.
        /// </summary>
        [NotNull]
        private readonly ICacheService<List<string>> domainMemoryService;

        /// <summary>
        /// The application settings.
        /// </summary>
        [NotNull]
        private readonly ApplicationSettings applicationSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="DisposableDomainsService" /> class.
        /// </summary>
        /// <param name="cacheService">The cache service.</param>
        /// <param name="applicationSettings">The application settings.</param>
        public DisposableDomainsService(
            [NotNull] ICacheService<List<string>> cacheService,
            [NotNull] ApplicationSettings applicationSettings)
        {
            this.domainMemoryService = cacheService;
            this.applicationSettings = applicationSettings;
        }

        /// <inheritdoc />
        public async Task<DisposableDomainResult> CheckDomainAsync(DisposableDomainRequestModel requestModel, CancellationToken cancellationToken)
        {
            var result = new DisposableDomainResult
            {
                Doc = this.applicationSettings.DomainsGitHubRepositoryUrl,
                Query = requestModel.Query,
                Result = new DisposableDomainMessage(),
            };

            /*Validation*/
            if (string.IsNullOrEmpty(requestModel.Query))
            {
                result.Result.Error = ErrorStates.NoText;
                return result;
            }

            if (requestModel.Query.Length > 255)
            {
                result.Result.Error = ErrorStates.QueryTooLong;
                return result;
            }

            var domain = string.Empty;

            var valueTuple = requestModel.Query.ParseDomain();
            if (valueTuple != null)
            {
                domain = valueTuple.Value.Item2;
            }

            /*Check*/
            var value = await this.domainMemoryService.GetOrSetAsync(cancellationToken).ConfigureAwait(false);

            var firstOrDefault = value?.FirstOrDefault(r => string.Compare(r, domain, StringComparison.CurrentCultureIgnoreCase) == 0);

            result.Result.IsDisposable = firstOrDefault != null;

            return result;
        }
    }
}
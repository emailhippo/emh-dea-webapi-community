﻿using System;
using System.Net.Mail;

namespace DeaIdentification.Infrastructure.Helpers
{
    public class ValidationHelper
    {
        public static bool IsEmailValid(string emailaddress)
        {
            try
            {
                var m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}

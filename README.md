# DEA Detector API - Community Edition.

## About

For further information, please see:

* [API Integration Documentation](https://www.disposable-email-detector.com)


## Installation
There is now a public Docker image should you wish to host this yourself.

See [here](https://cloud.docker.com/u/emailhippo/repository/registry-1.docker.io/emailhippo/dea-id-community-api-wesbos-v1) for further information.
